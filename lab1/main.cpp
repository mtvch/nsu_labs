#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

void bubble_sort(int* arr, int N) {
    for (int i = 0; i < N - 1; i++) {
        for (int j = 0; j < N - i - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

int main(int argc, char* argv[])
{
    struct timespec start, end;
    
    if (argc != 2) {
        std::cerr << "Wrong number of arguments" << std::endl;
        return 1;
    }

    // 70000 ~ 16 sec

    int N = atoi(argv[1]);

    if (N <= 0) {
        std::cerr << "Invalid input" << std::endl;
        return 1;
    }

    int* arr = new int[N];

    srand(time(NULL));
    for (int i = 0; i < N; i++) {
        arr[i] = rand() % N;
    }

    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    bubble_sort(arr, N);

    // for (int i = 0; i < N; i++) {
    //     std::cout << arr[i] << " ";
    // }
    // std::cout << std::endl;
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);

    std::cout << "done" << std::endl;

    delete [] arr;
    
    std::cout << "Time taken: ";
    std::cout << end.tv_sec - start.tv_sec + 0.000000001 * (end.tv_nsec - start.tv_nsec);
    std::cout << " sec" << std::endl;
    return 0;
}