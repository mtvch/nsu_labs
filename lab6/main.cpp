#include <libusb.h>
#include <iostream>
#include <string>

const int buff_size = 128;

void print_class_by_code(const uint8_t& code) {
    switch (code) {
    case 0x1:
        std::cout << "Audio" << std::endl;
        break;
    case 0x2:
        std::cout << "Communications and CDC Control" << std::endl;
        break;
    case 0x3:
        std::cout << "HID" << std::endl;
        break;
    case 0x5:
        std::cout << "Physical" << std::endl;
        break;
    case 0x6:
        std::cout << "Image" << std::endl;
        break;
    case 0x7:
        std::cout << "Printer" << std::endl;
        break;
    case 0x8:
        std::cout << "Mass Storage" << std::endl;
        break;
    case 0x9:
        std::cout << "Hub" << std::endl;
        break;
    case 0xA:
        std::cout << "CDC-Data" << std::endl;
        break;
    case 0xB:
        std::cout << "Smart Card" << std::endl;
        break;
    case 0xD:
        std::cout << "Content Security" << std::endl;
        break;
    case 0xE:
        std::cout << "Video" << std::endl;
        break;
    case 0x0F:
        std::cout << "Personal Healthcare" << std::endl;
        break;
    case 0x10:
        std::cout << "Audio/Video Devices" << std::endl;
        break;
    case 0x11:
        std::cout << "Billboard Device Class" << std::endl;
        break;
    case 0x12:
        std::cout << "USB Type-C Bridge Class" << std::endl;
        break;
    case 0xDC:
        std::cout << "Diagnostic Device" << std::endl;
        break;
    case 0xe0:
        std::cout << "Wireless Controller" << std::endl;
        break;
    case 0xef:
        std::cout << "Miscellaneous" << std::endl;
        break;
    case 0xfe:
        std::cout << "Application Specific" << std::endl;
        break;
    case 0xff:
        std::cout << "Vendor specific" << std::endl;
        break;
    default:
        std::cout << "Unknown" << std::endl;
        break;
    }
}

void print_interface_class(libusb_device* &dev) {
    libusb_config_descriptor* config;
    if (int code = libusb_get_config_descriptor(dev, 0, &config)) {
        std::string error = libusb_error_name(code);
        throw(code);
    }
    const libusb_interface* inter;
    const libusb_interface_descriptor * interdesc;
    inter = &config->interface[0];
    interdesc = &inter->altsetting[0];
    print_class_by_code(interdesc->bInterfaceClass);
    libusb_free_config_descriptor(config);
}

void print_dev_class(const uint8_t& code, libusb_device* &dev) {
    if (code == LIBUSB_CLASS_PER_INTERFACE) {
        std::cout << "\tEach interface specifies its own class information and all interfaces operate independently";
        std::cout << std::endl;
        std::cout << "\tFirst interface class: ";
        print_interface_class(dev);
    }    
    else {
        std::cout << "\tDevice Class: ";
        print_class_by_code(code);
    }
}

void print_dev_vid(const uint16_t& id) {
    std::cout << "\tVendor ID: " << std::hex << id << std::endl;
}

void print_dev_pid(const uint16_t& id) {
    std::cout << "\tProduct ID: " << std::hex << id << std::endl;
}

void print_dev_serial_number(libusb_device* &dev, libusb_device_descriptor &desc) {
    libusb_device_handle* dev_handle;
    if (libusb_open(dev, &dev_handle) < 0) {
        std::string error = "Error getting device handler";
        std::cout << "\t" << error << std::endl;
        return;
    }
    unsigned char* data = new unsigned char[buff_size + 1];
    if (int code = libusb_get_string_descriptor_ascii(dev_handle, desc.iSerialNumber, data, buff_size) < 0) {
        std::cout << "ERROR: " << libusb_error_name(code) << std::endl;
        delete[] data;
        libusb_close(dev_handle);
        std::string error = "Error getting descriptor ascii";
        throw(error);
    }
    std::cout << "\tSerial number: " << data << std::endl;
    delete[] data;
    libusb_close(dev_handle);
}

void print_dev(libusb_device* &dev, int number) {
    libusb_device_descriptor desc;
    if (libusb_get_device_descriptor(dev, &desc) < 0) {
        std::string error = "Can't get device descriptor";
        throw(error);
    }

    std::cout << number << "." << std::endl;
    print_dev_class(desc.bDeviceClass, dev);
    print_dev_vid(desc.idVendor);
    print_dev_pid(desc.idProduct);
    print_dev_serial_number(dev, desc);
    std::cout << std::endl;
}

bool print_devices(libusb_device**& devs, int cnt) {
    try {
        std::cout << "Devices:" << std::endl;
        for (int i = 0; i < cnt; i++) {
            print_dev(devs[i], i);
        }
    }
    catch (const std::string& error) {
        std::cerr << "\t" << error << std::endl;
        return false;
    }
}

int main() {
    
    libusb_device** devs;
    libusb_context* ctx = NULL;

    if (libusb_init(&ctx) < 0) {
        std::cerr << "Can't initialize libusb session" << std::endl;
        return 1;
    }

    libusb_set_debug(ctx, 3);

    int cnt = libusb_get_device_list(ctx, &devs);
    if (cnt < 0) {
        std::cerr << "Get device list error" << std::endl;
        libusb_exit(ctx);

        return 1;
    }

    if (!print_devices(devs, cnt)) {
        libusb_free_device_list(devs, 1);
        libusb_exit(ctx);

        return 1;
    }

    libusb_free_device_list(devs, 1);
    libusb_exit(ctx);

    return 0;
}
