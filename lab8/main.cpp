#include <iostream>
#include <list>
#include <ctime>
#include <cstdlib>
#include <iterator>
#include <limits>

const size_t K = 5;
const size_t N_min = 128 / sizeof(size_t);
const size_t N_max = 32 * 1024 * 1024 / sizeof(size_t);
const size_t start_step = 1024 / sizeof(size_t);
const size_t step_k = 2;
const size_t measurement_number = 5;

size_t* create_forward_array(size_t N) {
    size_t* arr = new size_t[N];
    for (size_t i = 0; i < N - 1; i++) {
        arr[i] = i + 1;
    }
    arr[N - 1] = 0;
    return arr;
}

size_t* create_backward_array(size_t N) {
    size_t* arr = new size_t[N];
    for (size_t i = 1; i < N; i++) {
        arr[i] = i - 1;
    }
    arr[0] = N - 1;
    return arr;
}

std::list<size_t>::iterator get_correct_random_iterator(std::list<size_t>& list, size_t i, size_t N) {
    size_t list_index = rand() % (N - i);
    std::list<size_t>::iterator it = list.begin();
    std::advance(it, list_index);

    return it;
}

size_t* create_random_array(size_t N) {
    size_t* arr = new size_t[N];
    std::list<size_t> list;
    for (size_t i = 1; i < N; i++) {
        list.push_back(i);
    }
    srand(time(NULL));
    size_t i = 0;
    for (size_t j = 0; list.size() > 0; j++) {
        std::list<size_t>::iterator it = get_correct_random_iterator(list, j, N - 1);
        arr[i] = *it;
        i = *it;
        list.erase(it);
    }
    arr[i] = 0;
    std::cout << std::endl;
    return arr;
}

void warm_up(size_t* arr, size_t N) {
    for (size_t i = 0, k = 0; i < N; i++) {
        k = arr[k];
    }
}

void run(size_t* arr, size_t N) {
    for (size_t i = 0, k = 0; i < N * K; i++) {
        k = arr[k];
    }
}

void measure_time(size_t* arr, size_t N) {
    unsigned long long diff = std::numeric_limits<unsigned long long>::max();
    warm_up(arr, N);
    for (size_t i = 0; i < measurement_number; i++) {
        union ticks{
            unsigned long long t64;
            struct s32 { long th, tl; } t32;
        } start, end;

        asm("rdtsc\n":"=a"(start.t32.th),"=d"(start.t32.tl));

        //run(arr, N);
        for (size_t i = 0, k = 0; i < N * K; i++) {
            k = arr[k];
        }

        asm("rdtsc\n":"=a"(end.t32.th),"=d"(end.t32.tl));

        if (end.t64 - start.t64 < diff) {
            diff = end.t64 - start.t64;
        }
    }
    std::cout << "\tSize: " << N * sizeof(size_t) << " Bytes" << std::endl;
    std::cout << "\tTicks for element: " << diff / (N * K) << std::endl;
    std::cout << std::endl;
}

size_t upower(size_t n, size_t deg) {
    if (deg == 0) {
        return 1;
    }
    size_t acc = n;
    for (size_t i = 0; i < deg; i++) {
        acc *= n;
    }
    return acc;
}

void measure_forward() {
    size_t N = N_min;
    std::cout << "Forward array:" << std::endl;
    for (size_t i = 1; N < N_max; i++) {
        size_t* arr = create_forward_array(N);
        measure_time(arr, N);
        N += start_step * upower(step_k, i);
    }
}

void measure_backward() {
    size_t N = N_min;
    std::cout << "Backward array:" << std::endl;
    for (size_t i = 1; N < N_max; i++) {
        size_t* arr = create_backward_array(N);
        measure_time(arr, N);
        N += start_step * upower(step_k, i);
    }
}

void measure_random() {
    size_t N = N_min;
    std::cout << "Random array:" << std::endl;
    for (size_t i = 1; N < N_max; i++) {
        size_t* arr = create_random_array(N);
        measure_time(arr, N);
        N += start_step * upower(step_k, i);
    }
}

int main() {
    measure_forward();
    measure_backward();
    measure_random();

    return 0;
}
