#ifndef STATUS_H
#define STATUS_H

#include <string>

enum class Status {
    OK,
    ERROR
};

#endif