#ifndef MATRIX_H
#define MATRIX_H

#include <cstdlib>
#include <vector>
#include "status.h"
#include <string>

class Matrix {
    private:
        size_t m_size;
        size_t m_row_length;
        float **m_data;
        Status m_status;
        std::string m_message;
        void set_error_status();
        void set_ok_status();
        void set_message(const std::string &message);
    public:
        size_t get_size() const;
        size_t get_row_length() const;
        Status get_status() const;
        const float** get_data() const;
        const std::string& get_message() const;
        void inverse();
        void transpose();
        Matrix(const size_t size, const size_t row_length);
        Matrix(const size_t size);
        Matrix(const Matrix& matrix);
        ~Matrix();
        Matrix& operator=(const Matrix &matrix);

        friend Matrix operator/(const Matrix &A, const float a);
};

#endif