#include <iostream>
#include <time.h>
#include <cstdlib>
#include <xmmintrin.h>
#include <limits>
#include <cmath>

struct Matrix {
  float** data;
  size_t N;  
};

Matrix* create_matrix(size_t N) {
    Matrix* matrix = new Matrix;
    matrix->N = N;
    matrix->data = new float*[N];
    srand(static_cast<unsigned>(time(0)));
    for (size_t i = 0; i < N; i++) {
        matrix->data[i] = new float[N];
        for (size_t j = 0; j < N; j++) {
            matrix->data[i][j] = static_cast<float>(rand()) / (static_cast<float>(rand()) - static_cast<float>(rand()));
        }
    }
    return matrix;
}

void print_matrix(Matrix* matrix) {
    for (size_t i = 0; i < matrix->N; i++) {
        for (size_t j = 0; j < matrix->N; j++) {
            std::cout << matrix->data[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void delete_matrix(Matrix* A) {
    for (size_t i = 0; i < A->N; i++) {
        delete[] A->data[i];
    }
    delete[] A->data;
}


Matrix* div_matrix_number(Matrix* matrix, float a) {
    Matrix* n_matrix = new Matrix;
    n_matrix->N = matrix->N;
    n_matrix->data = new float*[n_matrix->N];

    for (size_t i = 0; i < n_matrix->N; i++) {
        n_matrix->data[i] = new float[n_matrix->N];
        for (size_t j = 0; j < n_matrix->N; j++) {
            n_matrix->data[i][j] = matrix->data[i][j] / a;
        }
    }
    
    return n_matrix;
} 

Matrix* sub_matrix_new(Matrix* A, Matrix* B) {
    if (A->N != B->N) {
        std::cerr << "Different sizes" << std::endl;
        return NULL;
    }

    Matrix* C = new Matrix;
    C->N = A->N;
    C->data = new float*[C->N];

    for (size_t i = 0; i < C->N; i++) {
        C->data[i] = new float[C->N];
        for (size_t j = 0; j < C->N; j++) {
            C->data[i][j] = A->data[i][j] - B->data[i][j];
        }
    }

    return C;
}

Matrix* sum_matrix_new(Matrix* A, Matrix* B) {
    if (A->N != B->N) {
        std::cerr << "Different sizes" << std::endl;
        return NULL;
    }

    Matrix* C = new Matrix;
    C->N = A->N;
    C->data = new float*[C->N];

    for (size_t i = 0; i < C->N; i++) {
        C->data[i] = new float[C->N];
        for (size_t j = 0; j < C->N; j++) {
            C->data[i][j] = A->data[i][j] + B->data[i][j];
        }
    }

    return C;
}

void sum_matrix(Matrix* A, Matrix* B) {
    if (A->N != B->N) {
        std::cerr << "Different sizes" << std::endl;
        return;
    }

    for (size_t i = 0; i < A->N; i++) {
        for (size_t j = 0; j < A->N; j++) {
            A->data[i][j] = A->data[i][j] + B->data[i][j];
        }
    }
}

Matrix* mul_matrix(Matrix* A, Matrix* B) {
    if (A->N != B->N) {
        std::cout << "Different sizes" << std::endl;
        return NULL;
    }

    Matrix* C = new Matrix;
    C->N = A->N;
    C->data = new float*[C->N];

    for (size_t i = 0; i < C->N; i++) {
        C->data[i] = new float[C->N];

        for (size_t j = 0; j < C->N; j++) {
            float data = 0;
            for (size_t k = 0; k < C->N; k++) {
                data += A->data[i][k] * B->data[k][j]; 
            }
            C->data[i][j] = data;
        }
    }
    
    return C;
}

Matrix* create_I(size_t N) {
    Matrix* matrix = new Matrix;
    matrix->N = N;
    matrix->data = new float*[N];
    for (size_t i = 0; i < N; i++) {
        matrix->data[i] = new float[N];
        for (size_t j = 0; j < N; j++) {
            if (i == j) {
                matrix->data[i][j] = 1;
            }
            else {
                matrix->data[i][j] = 0;
            }
        }
    }
    return matrix;
}

float max_j(Matrix* A) {
    float max = std::numeric_limits<float>::min();
    for (int j = 0; j < A->N; j++) {
        float sum = 0;
        for (int i = 0; i < A->N; i++) {
            sum += fabs(A->data[i][j]);
        }
        if (sum > max) {
            max = sum;
        }
    }
    return max;
}

float max_i(Matrix* A) {
    float max = std::numeric_limits<float>::min();
    for (int i = 0; i < A->N; i++) {
        float sum = 0;
        for (int j = 0; j < A->N; j++) {
            sum += fabs(A->data[i][j]);
        }
        if (sum > max) {
            max = sum;
        }
    }
    return max;
}

Matrix* transpose(Matrix* A) {
    Matrix* A_t = new Matrix;
    A_t->N = A->N;
    A_t->data = new float*[A_t->N];

    for (size_t i = 0; i < A_t->N; i++) {
        A_t->data[i] = new float[A_t->N];
        for (size_t j = 0; j < A_t->N; j++) {
            A_t->data[i][j] = A->data[j][i];
        }
    }
    
    return A_t;
}

Matrix* copy(Matrix* A) {
    Matrix* C = new Matrix;
    C->N = A->N;
    C->data = new float*[C->N];

    for (size_t i = 0; i < C->N; i++) {
        C->data[i] = new float[C->N];
        for (size_t j = 0; j < C->N; j++) {
            C->data[i][j] = A->data[i][j];
        }
    }

    return C;
}

Matrix* inverse(Matrix* A, int M) {
    Matrix* I = create_I(A->N);
    
    float div_number = max_j(A) * max_i(A);

    Matrix* A_t = transpose(A);


    Matrix* B = div_matrix_number(A_t, div_number);

    Matrix* BA = mul_matrix(B, A);

    Matrix* R = sub_matrix_new(I, BA);

    Matrix* R_deg = copy(R);
    Matrix* Acc = I;
    
    for (int i = 0; i < M; i++) {
        sum_matrix(Acc, R_deg);
        R_deg = mul_matrix(R_deg, R);
    }

    Matrix* Result = mul_matrix(Acc, B);

    delete_matrix(Acc);
    delete_matrix(A_t);
    delete_matrix(B);
    delete_matrix(BA);
    delete_matrix(R);
    delete_matrix(R_deg);

    return Result;
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cerr << "Bad input" << std::endl;
        return 1;
    }

    int N = atoi(argv[1]);
    int M = atoi(argv[2]);

    if (N <= 0 || M <= 0) {
        std::cerr << "Bad input" << std::endl;
        return 1;
    }    

    struct timespec start, end;

    Matrix* A = create_matrix(N);

    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    Matrix* A_inversed = inverse(A, M);

    clock_gettime(CLOCK_MONOTONIC_RAW, &end);

    Matrix* C = mul_matrix(A, A_inversed);

    std::cout << "A:" << std::endl;
    print_matrix(A);
    
    std::cout << "A^-1:" << std::endl;
    print_matrix(A_inversed);

    std::cout << "A x A^-1:" << std::endl;
    print_matrix(C);

    std::cout << "Time taken: ";
    std::cout << end.tv_sec - start.tv_sec + 0.000000001 * (end.tv_nsec - start.tv_nsec);
    std::cout << " sec" << std::endl;

    delete_matrix(A);
    delete_matrix(A_inversed);
    delete_matrix(C);
    return 0;
}
