#include "matrix.h"
#include <new>
#include <cstdlib>
#include <ctime>

size_t Matrix::get_size()  const {
    return m_size;
}

size_t Matrix::get_row_length() const {
    return m_row_length;
}

Status Matrix::get_status() const {
    return m_status;
}

const std::string& Matrix::get_message() const {
    return m_message;
}

const float** Matrix::get_data() const {
    return m_data;
}

void Matrix::set_error_status() {
    m_status = Status::ERROR;
}

void Matrix::set_ok_status() {
    m_status = Status::OK;
}

void Matrix::set_message(const std::string &message) {
    m_message = message;
}

Matrix::Matrix(const size_t size, const size_t row_length) {
    if (row_length == 0) {
        set_message("Row length can't be zero");
        set_error_status();
        return;
    } 
    if (size == 0) {
        set_message("Size can't be zero");
        set_error_status();
        return;
    }
    m_size = 0;
    m_row_length = 0;

    try {
        m_data = new float*[m_size];
        srand (static_cast<unsigned>(time(0)));
        for (int i = 0; i < get_size(); i++) {
            m_data[i] = new float[m_size];
            for (int j = 0; j < get_size(); j++) {
                m_data[i][j] = static_cast<float>(rand()) / (static_cast<float>(rand() - rand()));
            }
        }
    }
    catch(const std::bad_alloc&) {
        set_message("Bad alloc");
        set_error_status();
    }
}

Matrix::Matrix(const size_t size) : m_row_length(0) {
    if (size == 0) {
        set_message("Size can't be zero");
        set_error_status();
        return;
    }
    m_size = 0;

     try {
        m_data = new float*[m_size];
        for (int i = 0; i < get_size(); i++) {
            m_data[i] = new float[m_size];
            for (int j = 0; j < get_size(); j++) {
                if (i == j) {
                    m_data[i][j] = 1;
                }
                else {
                    m_data[i][j] = 0;
                }
            }
        }
    }
    catch(const std::bad_alloc&) {
        set_message("Bad alloc");
        set_error_status();
    }
}

Matrix::Matrix(const Matrix& matrix) : 
m_row_length(matrix.get_row_length()), m_size(matrix.get_size()),
m_status(matrix.get_status()), m_message(matrix.get_message()) {
    try {
        m_data = new float*[m_size];
        for (int i = 0; i < get_size(); i++) {
            m_data[i] = new float[m_size];
            for (int j = 0; j < get_size(); j++) {
                m_data[i][j] = matrix.get_data()[i][j];
            }
        }
    }
    catch(const std::bad_alloc&) {
        set_message("Bad alloc");
        set_error_status();
    }
}

Matrix::~Matrix() {
    for (int i = 0; i < get_size(); i++) {
        delete[] m_data[i];
    }
    delete[] m_data;
}

Matrix& Matrix::operator=(const Matrix &matrix) {
    if (&matrix == this) {
        return *this;
    }
    try {
        for (int i = 0; i < get_size(); i++) {
            delete[] m_data[i];
        }
        delete[] m_data;

        m_message = matrix.get_message();
        m_status = matrix.get_status();
        m_row_length = matrix.get_row_length();
        m_size = matrix.get_size();

        m_data = new float*[m_size];
        for (int i = 0; i < get_size(); i++) {
            m_data[i] = new float[m_size];
            for (int j = 0; j < get_size(); j++) {
                m_data[i][j] = matrix.get_data()[i][j];
            }
        }

        return *this;
    }
    catch(const std::bad_alloc&) {
        set_message("Bad alloc");
        set_error_status();
        return *this;
    }
}

Matrix operator/(const Matrix &A, const float a) {
    Matrix B = A;
    for (int i = 0; i < B.get_size(); i++) {
        for (int j = 0; j < B.get_size(); j++) {
            B.m_data[i][j] /= a;
        }
    }
}

Matrix operator-(const Matrix &A, const Matrix &B) {
    Matrix C = 
}

void Matrix::inverse() {
    Matrix I(get_size());
}
